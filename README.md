# Parkdown
– a simple recursive descent Markdown parser for PHP *(version >= 8.1)*

![Markdown is a simple markup language](./docs/logo_parkdown.svg)

## Specification

### Supported block types
Parkdown currently support the following block types:

* codeblocks *(with the ability to specify a language for the code block)*
* tables *(with alignment specification)*
* paragraphs
* block quotes
* lists *(like this one)*
	* also nested
* horizontal rules `---`

### Supported inline types
Parkdown currently support the following block types:

* bold text (`**bold**`)
* italic text (`*italic*`)
* code snippets
* images (`![alt text](src url)`)
* links (`[link text][url or reference]`)

### Additional functionality

* references (`[marker]: URL`)

## Examples
### Paragraphs
```markdown
A simple paragraph can contain **bold text**, `inline codeblocks` and *italic text*. We can also link [with a direct url][https://google.com] *(i.e. to google)*
or via reference to [a later defined url][massivedynamic], if we so desire.
```

A simple paragraph can contain **bold text**, `inline codeblocks` and *italic text*. We can also link [with a direct url](https://google.com) *(i.e. to google)*
or via reference to [a later defined url][massivedynamic], if we so desire.

Paragraphs can be annotated with `id` and `class` attributes:

```markdown
Paragraphs can be annotated with ids and classes {.thisIsAClass, .anotherClass, #thisIsAnID}
```

results in

Paragraphs can be annotated with ids and classes {.thisIsAClass, .anotherClass, #thisIsAnID}

```html
<p class="thisIsAClass anotherClass" id="thisIsAnID">
	Paragraphs can be annotated with ids and classes
</p>
```

### Images
```markdown
![this is an alt text](https://images.unsplash.com/photo-1571171637578-41bc2dd41cd2?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&h=300&w=1740&q=80\)
```

![this is an alt text](https://images.unsplash.com/photo-1571171637578-41bc2dd41cd2?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&h=300&w=1740&q=80)

### Horizontal rules
```markdown
---
```

---
### Block quotes
```markdown
> Only two things are infinite,
> the universe and human stupidity,
> i am not totally shure about the universe, though...
>    – Albert Einstein
```


> Only two things are infinite,
> the universe and human stupidity,
> i am not totally shure about the universe, though...
>    – Albert Einstein

### Code blocks
```markdown

\`\`\`php
	function main(int $argc, array $argv) : int {
		echo "Hello World!";

		return 0;
	}
\`\`\`

```

```php
	function main(int $argc, array $argv) : int {
		echo "Hello World!";

		return 0;
	}
```

### Tables
```markdown
| Product name | Amount   | Price  |
|--------------|:--------:|-------:|
| Football     |      7   | $18,00 |
| Golfball     |      122 | $7,00  |
| Fooseball    |      355 | $1,00  |
| Puck         |      58  | $12,00 |
```

| Product name | Amount   | Price  |
|--------------|:--------:|-------:|
| Football     |      7   | $18,00 |
| Golfball     |      122 | $7,00  |
| Fooseball    |      355 | $1,00  |
| Puck         |      58  | $12,00 |

### References

```markdown
[massivedynamic]: https://massivedynamic.eu
```

[massivedynamic]: https://massivedynamic.eu

## Usage
Simply construct an new `parkdown\Parkdown` object and pass the Markdown source code to it's constructor. The parsed `DOMDocument` or it's `HTML` output can then be retrieved through the `::html()` and `::tree()` member functions.

**Example**

```php
	use parkdown\Parkdown;

	$source = "
		This is a **bold** word in a paragraph.
	";

	$parser = new Parkdown($source);
	$tree   = $parser->tree();

	print_r($tree);
	echo $parser->html();
```

## Testing
Unit tests can be run via `composer`:

```
	composer test
```