<?php

require __DIR__."/vendor/autoload.php";


$source = file_get_contents(dirname(__FILE__)."/README.md");

echo "
<style>
	body {
		padding: 4rem;
		margin: 0;
		font-family: sans-serif;
	}

	img {
		max-width: 100%;
	}

	code {
		word-break: break-word;
		white-space: break-spaces;
	}

	table {
		width: 100%;
	}
</style>
";

$Instance = new parkdown\Parkdown($source);
echo $Instance->html();