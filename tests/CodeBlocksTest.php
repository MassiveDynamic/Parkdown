<?php declare(strict_types=1);

use PHPUnit\Framework\TestCase;

final class CodeBlocksTest extends TestCase {
	public function testCodeblocksParseCorrectly() : void {
		$source = "
```
	this is a code block
```
		";
		$target = "
<pre>
	<code>
		this is a code block
	</code>
</pre>
		";

		[$source, $result] = createTest($source, $target);
		$this->assertEquals($source, $result);
	}
	
	public function testLanguageAnnotationParsesCorrectly() : void {
		$source = "
```php
	public function testLanguageAnnotationParsesCorrectly() : bool {
		return true;
	}
	
```
		";
		$target = "
<pre data-lang=\"php\" class=\"language-php\">
	<code data-lang=\"php\" class=\"language-php\">
		public function testLanguageAnnotationParsesCorrectly() : bool {
			return true;
		}
	</code>
</pre>
		";

		[$source, $result] = createTest($source, $target);
		$this->assertEquals($source, $result);
	}
}