<?php declare(strict_types=1);

use PHPUnit\Framework\TestCase;

final class ListsTest extends TestCase {
	public function testUnorderedListsParseCorrectly() : void {
		$source = "

* Listpoint A
* Listpoint B
	* Subpoint B1
	* Subpoint B2
* Listpoint C

		";
		$target = "
<ul>
	<li> Listpoint A</li>
	<li>
		Listpoint B
		<ul>
			<li> Subpoint B1</li>
			<li> Subpoint B2</li>
		</ul>
	</li>
	<li> Listpoint C</li>
</ul>
		";
		[$source, $result] = createTest($source, $target);
		$this->assertEquals($source, $result);
	}
	
	public function testOrderedListsParseCorrectly() : void {
		$source = "

1. Listpoint A
2. Listpoint B
	1. Subpoint B1
	2. Subpoint B2
3. Listpoint C

		";
		$target = "
<ol>
	<li> Listpoint A</li>
	<li>
		Listpoint B
		<ol>
			<li> Subpoint B1</li>
			<li> Subpoint B2</li>
		</ol>
	</li>
	<li> Listpoint C</li>
</ol>
		";
		[$source, $result] = createTest($source, $target);
		$this->assertEquals($source, $result);
	}
}