<?php declare(strict_types=1);

use PHPUnit\Framework\TestCase;

final class TablesTest extends TestCase {
	public function testTableParsesCorrectly() : void {
		$source = "
| Product | Amount | Price  |
|:--------|:------:|-------:|
| Part A  | 5      | 20,00€ |
| Editor  | 100    | 39,99€ |

		";
		$target = "
<table>
			<tr>
				<th style=\"text-align: left\">Product</th>
				<th style=\"text-align: center\">Amount</th>
				<th style=\"text-align: right\">Price</th>
			</tr>
			<tr>
				<td style=\"text-align: left\">Part A</td>
				<td style=\"text-align: center\">5</td>
				<td style=\"text-align: right\">20,00&euro;</td>
			</tr>
			<tr>
				<td style=\"text-align: left\">Editor</td>
				<td style=\"text-align: center\">100</td>
				<td style=\"text-align: right\">39,99&euro;</td>
			</tr>
</table>		
		";

		[$source, $result] = createTest($source, $target);
		$this->assertEquals($source, $result);
	}
}