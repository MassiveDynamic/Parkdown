<?php declare(strict_types=1);

use parkdown\Parkdown;

function createTest(string $source, string $target) : array {
	$strip = function(string $string) : string {
		return trim(preg_replace("/\s+/", "", $string));
	};

	$parkdown = new Parkdown($source);
	return [$strip($target), $strip($parkdown->html())];
}