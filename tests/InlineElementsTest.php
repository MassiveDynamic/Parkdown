<?php declare(strict_types=1);

use PHPUnit\Framework\TestCase;

final class InlineElementsTest extends TestCase {
	public function testBoldParsesCorrectly() : void {
		$source = "
		
		Lorem **ipsum** dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor.

		";
		$target = "
		<p>Lorem <b>ipsum</b> dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor.</p>
		";

		[$source, $result] = createTest($source, $target);
		$this->assertEquals($source, $result);
	}
	
	public function testItalicParsesCorrectly() : void {
		$source = "
		
		Lorem **ipsum** dolor sit amet, *consetetur* sadipscing elitr, sed diam nonumy eirmod tempor.

		";
		$target = "
		<p>Lorem <b>ipsum</b> dolor sit amet, <i>consetetur</i> sadipscing elitr, sed diam nonumy eirmod tempor.</p>
		";

		[$source, $result] = createTest($source, $target);
		$this->assertEquals($source, $result);
	}

	public function testCodeSnippetsParseCorrectly() : void {
		$source = "
		
		Lorem **ipsum** dolor sit amet, *consetetur* sadipscing `elitr`, sed diam nonumy eirmod tempor.

		";
		$target = "
		<p>Lorem <b>ipsum</b> dolor sit amet, <i>consetetur</i> sadipscing <code>elitr</code>, sed diam nonumy eirmod tempor.</p>
		";

		[$source, $result] = createTest($source, $target);
		$this->assertEquals($source, $result);
	}

	public function testImagesParseCorrectly() : void {
		$source = "
		![an image](https://massivedynamic.eu/wp-content/themes/massivedynamic/logo_massive_dynamic.png)
		Lorem ipsum dolor sit amet.

		";
		$target = "
		<p>
			<img alt=\"an image\" src=\"https://massivedynamic.eu/wp-content/themes/massivedynamic/logo_massive_dynamic.png\">
			Lorem ipsum dolor sit amet.
		</p>
		";

		[$source, $result] = createTest($source, $target);
		$this->assertEquals($source, $result);
	}
	
	public function testLinksParseCorrectly() : void {
		$source = "
		
		Lorem **ipsum** dolor sit amet, *consetetur* sadipscing `elitr`, [sed diam](https://massivedynamic.eu) nonumy eirmod tempor.

		";
		$target = "
		<p>
			Lorem <b>ipsum</b> dolor sit amet, <i>consetetur</i> sadipscing <code>elitr</code>,
			<a href=\"https://massivedynamic.eu\">
				sed diam
			</a>
			nonumy eirmod tempor.
		</p>
		";

		[$source, $result] = createTest($source, $target);
		$this->assertEquals($source, $result);
	}
}