<?php declare(strict_types=1);

use PHPUnit\Framework\TestCase;

final class GenericBlocksTest extends TestCase {
	public function testParagraphParsesCorrectly() : void {
		$source = "
this is a paragraph
		";
		$target = "
<p>
	this is a paragraph
</p>
		";

		[$source, $result] = createTest($source, $target);
		$this->assertEquals($source, $result);
	}

	public function testBlockquotesParseCorrectly() : void {
		$source = "
> this is
>
> a blockquote

";
		$target = "
<blockquote>
	this is <br><br>
	a blockquote<br>
</blockquote>
		";

		[$source, $result] = createTest($source, $target);
		$this->assertEquals($source, $result);
	}

	public function testHorizontalRuleParsesCorrectly() : void {
		$source = "
---
		";
		$target = "<hr>";

		[$source, $result] = createTest($source, $target);
		$this->assertEquals($source, $result);
	}

	public function testParagraphAnnotations() : void {
		$source = "
		
this is an annotated paragraph {.classA, .classB}
		
		";
		$target = "
		<p class=\"classA classB\">
			this is an annotated paragraph
		</p>
		";

		[$source, $result] = createTest($source, $target);
		$this->assertEquals($source, $result);
	}
}