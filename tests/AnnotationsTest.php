<?php declare(strict_types=1);

use PHPUnit\Framework\TestCase;

final class AnnotationsTest extends TestCase {
	public function testAnnotationsDoNotBreakHeadings() : void {
		$source = "
# This is an H1 {.withAClass}
## This is an H2 {.withAClass}
### This is an H3 {.withAClass}
#### This is an H4 {.withAClass}
##### This is an H5 {.withAClass}
		";
		$target = "
		<h1>This is an H1</h1>
		<h2>This is an H2</h2>
		<h3>This is an H3</h3>
		<h4>This is an H4</h4>
		<h5>This is an H5</h5>
		";

		[$source, $result] = createTest($source, $target);
		$this->assertEquals($source, $result);
	}

	public function testAnnotationsDoNotBreakLists() : void {
		$source = "

* this is a list
* with annotations {#someID}

1. this is an ordered list
	1. with an annotation {.someClass}

";
		$target = "
		<ul>
			<li>this is a list</li>
			<li>with annotations</li>
		</ul>
		<ol>
			<li>
				this is an ordered list
				<ol>
					<li>with an annotation</li>
				</ol>
			</li>
		</ol>
		";

		[$source, $result] = createTest($source, $target);
		$this->assertEquals($source, $result);
	}

	public function testAnnotationsDoNotBreakCodeblocks() : void {
		$source = "
```
		this is a code block
``` {.someClass}
";
		$target = "
		<pre>
			<code>this is a code block</code>
		</pre>
		";

		[$source, $result] = createTest($source, $target);
		$this->assertEquals($source, $result);
	}
}