<?php declare(strict_types=1);

use PHPUnit\Framework\TestCase;

final class ReferencesTest extends TestCase {
	public function testSimpleReferencesAreResolvedCorrectly() : void {
		$source = "
this text uses [a simple reference][1]

[1]: https://massivedynamic.eu
		";
		$target = "
<p>
	this text uses <a href=\"https://massivedynamic.eu\">a simple reference</a>
</p>
		";

		[$source, $result] = createTest($source, $target);
		$this->assertEquals($source, $result);
	}
	
	public function testNamedReferencesAreResolvedCorrectly() : void {
		$source = "
this text uses [a simple reference][REF] and does it [twice][REF]

[REF]: https://massivedynamic.eu
		";
		$target = "
<p>
	this text uses <a href=\"https://massivedynamic.eu\">a simple reference</a>
	and does it <a href=\"https://massivedynamic.eu\">twice</a>
</p>
		";

		[$source, $result] = createTest($source, $target);
		$this->assertEquals($source, $result);
	}
}