<?php declare(strict_types=1);

use PHPUnit\Framework\TestCase;

final class HeadingsTest extends TestCase {
	public function testH1ParsesCorrectly() : void {
		$source = "
# This is an H1
		";
		$target = "
		<h1>This is an H1</h1>
		";

		[$source, $result] = createTest($source, $target);
		$this->assertEquals($source, $result);
	}
	
	public function testH2ParsesCorrectly() : void {
		$source = "
## This is an H2
		";
		$target = "
		<h2>This is an H2</h2>
		";

		[$source, $result] = createTest($source, $target);
		$this->assertEquals($source, $result);
	}

	public function testH3ParsesCorrectly() : void {
		$source = "
### This is an H3
		";
		$target = "
		<h3>This is an H3</h3>
		";

		[$source, $result] = createTest($source, $target);
		$this->assertEquals($source, $result);
	}

	public function testH4ParsesCorrectly() : void {
		$source = "
#### This is an H4
		";
		$target = "
		<h4>This is an H4</h4>
		";

		[$source, $result] = createTest($source, $target);
		$this->assertEquals($source, $result);
	}

	public function testH5ParsesCorrectly() : void {
		$source = "
##### This is an H5
		";
		$target = "
		<h5>This is an H5</h5>
		";

		[$source, $result] = createTest($source, $target);
		$this->assertEquals($source, $result);
	}
}