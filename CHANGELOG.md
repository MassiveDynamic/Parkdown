# Changelog

## Version 1.2.0
The *"annotation release"*

### Added
* paragraph annotations *(now can set HTML ids and classes with `{.someClass, #someId}` syntax)*
* unit tests for annotation
* unit for headings *(`h1` to `h5`)*

### Removed
* Testing code in `index.php`

## Version 1.1.2
No major changes introduced, getting ready for release

### Changed
* added compatibility for `prism` code highlighting inside code blocks

## Version 1.1.0
No major changes introduced, getting ready for release

### Added
* unit tests via `PHPUnit`

### Changed
* Parser does not generate unnessecary empty paragraphs anymore
* Fix: parse does not get stuck on blockquotes that are not followed by an empty
  line anymore

### Removed
* "Monkey test" `.md` files in `tests` folder

## Version 1.0.0
Basic implementation