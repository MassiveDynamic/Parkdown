<?php declare(strict_types=1);

namespace parkdown;

use DOMDocument;

class Parkdown {
	private string      $sourceCode;
	private DOMDocument $tree_;

	public function __construct(string $sourceCode) {
		$this->sourceCode = $sourceCode;

		$lexer  = new Lexer($this->sourceCode);
		$parser = new Parser($lexer->tokenize());
	
		$this->tree_ = $parser->parse();
	}

	public function tree() : DOMDocument {
		return $this->tree_;
	}

	public function html() : string {
		return $this->tree_->saveHTML();
	}
}