<?php declare(strict_types=1);

namespace parkdown;

enum TokenType {
	case HASH     ;
	case ASTERISK ;
	case TEXT     ;
	case DOT      ;
	case MINUS    ;
	case NUMBER   ;
	case EOL      ;
	case EOF      ;
	case BACKTICK ;
	case LBRACKET ;
	case RBRACKET ;
	case LPAREN   ;
	case RPAREN   ;
	case BANG     ;
	case COLON    ;
	case BACKSLASH;
	case PIPE     ;
	case GT       ;
	case TAB      ;
	case LBRACE   ;
	case RBRACE   ;
}

class Token {
	public TokenType $type;
	public string    $data;

	public function __construct(TokenType $type, string $data = "") {
		$this->type = $type;
		$this->data = $data;
	}
}